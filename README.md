# Quick documentation on steps for nanopore assembly

The reads were first subsampled using [rasusa](https://github.com/mbhall88/rasusa), followed by the assembly of the subsampled reads using [Dragonflye](https://github.com/rpetit3/dragonflye).

### Install conda environment (containing rasusa and Dragonflye)
```
conda env create --name dragonflye --file dragonflye.yml
```

### Create interactive session
```
screen -S nanopore
srun --time=18:00:00 --mem=60G -c 4 --pty bash
conda activate dragonflye
```

### Concatenate reads 
```
cat /path/to/fastq_pass/barcode${n}/*.gz > barcode${n}_pass_cat.fastq.gz
```
### Subsample reads using rasusa to 40x coverage, genome-size 5000000
```
rasusa --input barcode${n}_pass_cat.fastq.gz --coverage 40 --genome-size 5000000 --output barcode${n}_pass_cat_subsample.fastq.gz --output-type g
```
### Run Dragonflye using the subsamples reads
```
dragonflye --reads fastq_pass/barcode${n}_pass_cat_subsample.fastq.gz --outdir dragonfye_assemblies/barcode${n} --prefix barcode${n}
```
### Main output (assembly fasta file containing the contigs)
```
dragonfye_assemblies/barcode${n}/barcode${n}.fa
```
